from rest_framework import serializers


class TaskSerializer(serializers.Serializer):
    """
    Сериализатор задач (выгрузок) имеющихся на сервисе
    """

    unload_rid = serializers.IntegerField(label="Идентификатор выгрузки", min_value=1)
    databases = serializers.ListField(child=serializers.RegexField(r"[A-Z0-9]"), required=False)
    enabled = serializers.BooleanField(label="Вкл./Выкл.", help_text="true - включить задачу, false - выключить")


class StateSerializer(serializers.Serializer):
    """
    Сериализатор состояния сервиса
    """

    enabled = serializers.BooleanField(
        label="Вкл./Выкл.",
        help_text="true - включить сервис, false - полностью выключить (со всеми задачами)",
        required=False,
    )
    tasks = TaskSerializer(label="Задачи", help_text="Задачи (выгрузки) на сервисе", many=True, required=False)

    def validate(self, data):
        if "enabled" not in data or "tasks" not in data or not data["tasks"]:
            raise serializers.ValidationError(
                "В запросе должно быть либо поле 'enabled' либо хотя бы одно значение 'tasks'."
            )
        elif "enabled" in data and "tasks" in data and data["tasks"]:
            raise serializers.ValidationError("Поля 'enabled' и 'tasks' не разрешены вместе.")
        return data
