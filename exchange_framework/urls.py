from django.urls import path

from .views import state

urlpatterns = [
    path("api/v1/state/", state),
]
