from django.conf import settings
from django.core.checks import Error


def check_settings(app_configs, **kwargs):
    errors = []
    try:
        settings.EXCHANGE_SERVICE
    except AttributeError:
        errors.append(
            Error(
                "В настройках отсутствует EXCHANGE_SERVICE",
                hint="Добавьте EXCHANGE_SERVICE в settings.py.",
                obj=settings,
                id="exchange-framework.E001",
            )
        )
    try:
        name = settings.EXCHANGE_SERVICE["name"]
        assert isinstance(name, str) and len(name) > 0
    except KeyError:
        errors.append(
            Error(
                "В настройках EXCHANGE_SERVICE отсутствует поле name",
                hint="В settings.py добавьте наименование сервиса name в EXCHANGE_SERVICE.",
                obj=settings,
                id="exchange-framework.E002",
            )
        )
    except AssertionError:
        errors.append(
            Error(
                "Поле name EXCHANGE_SERVICE имеет неправильный формат",
                hint="Наименование сервиса name должно быть строкой.",
                obj=settings,
                id="exchange-framework.E003",
            )
        )
    try:
        srv_rid = settings.EXCHANGE_SERVICE["srv_rid"]
        assert srv_rid > 0
    except KeyError:
        errors.append(
            Error(
                "В настройках EXCHANGE_SERVICE отсутствует поле srv_rid",
                hint="В settings.py добавьте идентификатор сервиса srv_rid в EXCHANGE_SERVICE.",
                obj=settings,
                id="exchange-framework.E004",
            )
        )
    except (TypeError, AssertionError):
        errors.append(
            Error(
                "Поле srv_rid в EXCHANGE_SERVICE имеет неправильный формат",
                hint="srv_rid должно быть целое число больше 0.",
                obj=settings,
                id="exchange-framework.E005",
            )
        )
    return errors
