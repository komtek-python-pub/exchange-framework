from django.apps import AppConfig
from django.core.checks import Tags, register


class ExchangeFrameworkConfig(AppConfig):
    name = "exchange_framework"
    verbose_name = "Фреймворк для сервисов обмена"

    def ready(self):
        from .checks import check_settings

        register(check_settings, Tags.compatibility)
