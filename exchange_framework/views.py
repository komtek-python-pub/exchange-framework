import datetime
import json

from django_celery_beat.models import PeriodicTask
from django_celery_results.models import TaskResult

from rest_framework.authentication import BasicAuthentication  # noqa
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.response import Response

from django.conf import settings

from .serializers import StateSerializer
from .utils import task_updated_message


@api_view(["GET", "POST"])
@authentication_classes([])
def state(request):
    """
    Получение и включение/выключение состояния сервиса и списка задач (документов)
    """
    objs = []
    for obj in PeriodicTask.objects.all():
        try:
            kwargs = json.loads(obj.kwargs)
        except json.JSONDecodeError:
            kwargs = {}
        if "db_name" in kwargs and "unload_rid" in kwargs:
            obj.db_name = kwargs["db_name"]
            obj.unload_rid = kwargs["unload_rid"]
            objs.append(obj)

    if request.method == "POST":
        serializer = StateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if "enabled" in serializer.validated_data:
            enabled = serializer.validated_data["enabled"]
            ids = [obj.pk for obj in objs]
            updated = PeriodicTask.objects.filter(pk__in=ids).update(enabled=enabled)
            return Response({"message": task_updated_message(updated, enabled)})

        tasks = serializer.validated_data["tasks"]
        temp = []
        for obj in objs:
            temp[obj.db_name, obj.unload_rid] = [obj]
            temp.setdefault((None, obj.unload_rid), [])
            temp[None, obj.unload_rid].append(obj)
        enabled, disabled = [], []
        for task in tasks:
            try:
                databases = task["databases"]
            except KeyError:
                databases = [None]
            for db_name in databases:
                obj = temp[(db_name, task["unload_rid"])]
                if task["enabled"]:
                    obj.enabled = True
                    enabled.extend(obj)
                else:
                    obj.enabled = False
                    disabled.extend(obj)
        PeriodicTask.objects.bulk_update(enabled + disabled, ["enabled"])
        messages = [task_updated_message(len(enabled), True).rstrip(".")]
        messages.append(task_updated_message(len(disabled), False).rstrip("."))
        message = f"{', '.join(messages)}."
        return Response({"message": message})

    ids = [obj.pk for obj in objs]
    results = {obj.pk: obj.as_dict() for obj in TaskResult.objects.filter(task_id__in=ids)}
    tasks = []
    enabled = False
    for obj in objs:
        result = results.get(obj.pk, {})
        task = {
            "name": obj.name,
            "unload_rid": obj.unload_rid,
            "db_name": obj.db_name,
            "enabled": obj.enabled,
            "last_start": obj.last_run_at,
            "last_finish": result.get("date_done", None),
            "status": result.get("status", None),
            "message": result.get("result", None) or result.get("traceback", None),
            "interval": str(obj.interval or obj.crontab or obj.solar or obj.clocked),
        }
        if obj.enabled:
            enabled = True
            if obj.interval and obj.last_run_at:
                next_run = obj.last_run_at + datetime.timedelta(**{obj.interval.period: obj.interval.every})
            elif obj.crontab:
                next_run = str(obj.crontab)
            elif obj.solar:
                next_run = str(obj.solar)
            elif obj.clocked:
                next_run = str(obj.clocked)
            else:
                next_run = None
            task["next_run"] = next_run
        task = {k: v for k, v in task.items() if v is not None}
        tasks.append(task)
    data = {
        "name": settings.EXCHANGE_SERVICE["name"],
        "srv_rid": settings.EXCHANGE_SERVICE["srv_rid"],
        "enabled": enabled,
        "tasks": tasks,
    }
    return Response(data)
