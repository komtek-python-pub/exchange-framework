def task_updated_message(updated, enabled):
    if enabled:
        if updated == 1:
            message = "{} задача запущена."
        if 1 < updated < 5:
            message = "{} задачи запущено."
        else:
            message = "{} задач запущено."
    else:
        if updated == 1:
            message = "{} задача остановлена."
        if 1 < updated < 5:
            message = "{} задачи остановлено."
        else:
            message = "{} задач остановлено."
    return message.format(updated)
