## Фреймворк для сервисов обмена.

Предназначен для единого подхода к реализации сервисов обмена (API, настройки, результаты выгрузки, и т.д.).

**Версия**: 0.0.1 (Alpha)

### Возможности

- Единое API для всех сервисов выгрузок
- Единые настройки для всех сервисов выгрузок
- Возможность работы с несколькими БД МО
- Выгрузка по расписанию
- Выгрузка по синхронному API
- Выставление результатов выгрузки в lpu.iemk_documents
- Управление выгрузками (включение/выключение задач) **[DONE]**
- Перезагрузка выбранных выгрузок
- Получение установленной версии сервиса
- Мониториг состояния (сервис не завис, есть коннект до БД, что-то ещё важное)

### Зависимости

- Django (>=3.1)
- Django REST Framework (>=3.12)
- Celery (>=5.0)
- django-celery-beat (>=2.2)
- django-celery-results (>=2)

## Установка

Установка последней версии:

    $ pip install git+ssh://git@gitlab.com/komtek-python-dev/exchange-framework.git@0.0.1#egg=exchange_framework

или добавьте в `requirements.txt`:

    ...
    git+ssh://git@gitlab.com/komtek-python-dev/exchange-framework.git@0.0.1#egg=exchange_framework

и запустите:

    $ pip install -r requirements.txt

При использовании Docker добавьте в cвой `Dockerfile` ssh ключ `komtek-deploy` для установки пакета из репозитория:

```
...
RUN mkdir ~/.ssh && \
    mv komtek-deploy ~/.ssh/id_rsa && \
    chmod =600 ~/.ssh/id_rsa && \
    ssh-keyscan -t rsa gitlab.com > ~/.ssh/known_hosts && \
    pip install -U pip && \
    pip install -r requirements.txt && \
    rm -rf ~/.ssh
...
```

Ключ для деплоя `komtek-deploy`: https://kb.pkzdrav.ru/pages/viewpage.action?pageId=51908910


## Настройка

>❗️Настройка Django + Celery: https://docs.celeryproject.org/en/stable/django/first-steps-with-django.html

Добавьте в настройках `settings.py`:

```python
INSTALLED_APPS = [
    ...
    "exchange_framework",
]
...
EXCHANGE_SERVICE = {
    "name": "Сервис выгрузки в УдКон ВИМИС",
    "srv_rid": 27,
}
```

Добавьте в файл `urls.py`:

```python
urlpatterns = [
    path("", include("exchange_framework.urls")),
    ...
]
```

Запустите миграцию:

    $ python manage.py makemigrations
    $ python migrate

Обновите сигнатуру функции Celery задачи сервиса в файле `tasks.py`:

```python
@shared_task
def some_task(db_name, unload_rid, **kwargs):
    ...
```

Добавьте в административной панели в периодическую задачу приложения django-celery-beat аргументы для вызова вашей задачи (обязательно):

    Arguments:
    ...
    Keyword Arguments: {"db_name":"NVONKO","unload_rid":12}
    ...

или через `python manage.py shell`:

```python
from django_celery_beat.models import PeriodicTask
PeriodicTask.objects.filter(pk=<ID вашей задачи>).update(kwargs='{"db_name":"NVONKO","unload_rid":12}')
```

## API

### 1. Управление выгрузками (включение/выключение задач)

#### 1.1 Получение состояния сервиса и списка задач (выгрузок)

    GET /api/v1/state

```json
{
    "name": "Сервис выгрузки в УдКон ВИМИС",
    "srv_rid": 27,
    "enabled": true,
    "tasks": [
        {
            "name":"smsv1",                        // Наименование (например, вид выгружаемого документа)
            "unload_rid": 35,
            "db_name": "NVONKO",                   // База данных
            "enabled": true,                       // true - задача включена, false - выключена
            "last_start": "2021-01-01T12:00:00",   // Дата и время запуска последней задачи
            "last_finish": "2021-01-01T12:15:00",  // Дата и время завершения последней задачи
            "status": "FAILURE",                   // Необязательный. Результат последнего выполнения
            "message":"Exception: blah-blah-blah", // Необязательный. Сообщение об ошибке работы задания
            "next_run": "2021-01-01T13:00:00",     // Необязательный. Время следующего запуска (если задание включено).
            "interval": "Каждый час"               // Необязательный. Интервал выполнения задания.
        },
        {
            "name": "smsv2",
            "unload_rid": 39,
            "db_name": "NVDS1",
            "enabled": true,
            "last_run": "2021-01-01T12:00:00",
            "status": "SUCCESS",
            "message": "Successfull",
            "interval": "Каждые сутки в 02:00"
        }
    ]
}
```

#### 1.2 Изменение состояния сервиса и списка задач (выгрузок)

    POST /api/v1/state

```json
{
    "enabled": true,  // true - включить сервис, false - полностью выключить (со всеми задачами)
    "tasks": [
        {
            "unload_rid": 35,
            "databases": ["NVONKO", "NVDS1"], // Необязательный. Список баз данных (отсутствие списка означает все БД)
            "enabled": true // включить выгрузку документа с типом lpu.tmpl_unload = 35 (SMSV1) Направление на оказание медицинских услуг.
        }
    ]
}
// Не разрешено чтобы было в запросе сразу и enabled и tasks.
```

## Ссылки

- API управления выгрузками https://kb.pkzdrav.ru/pages/viewpage.action?pageId=51911163
